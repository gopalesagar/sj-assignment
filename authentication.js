module.exports = {
	authenticateApiKey: function(req, res, next) {
		if(process.env.API_KEY === req.headers['x-api-key']) {
			next()
		} else {
			res.status(401).send('Unauthorized access')
		}
	}
}