var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var ev = require('express-validation')
const YAML = require('yamljs');
var swaggerUi = require('swagger-ui-express')
const swaggerDocument = YAML.load('./swagger.yml');
const auth = require('./authentication.js');

var indexRouter = require('./routes/index');
var fizzBuzzRouter = require('./routes/fizzBuzz');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/', indexRouter);
app.use('/fizzBuzz', auth.authenticateApiKey, fizzBuzzRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // Handling validation errors here
  if (err instanceof ev.ValidationError) {
    return res.status(err.status).send(err.errors)
  }

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
