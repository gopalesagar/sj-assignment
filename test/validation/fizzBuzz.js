var Joi = require('joi');

module.exports = {
	params: {
		count: Joi.number().greater(0).required()
	}
}