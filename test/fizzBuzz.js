var expect  = require('chai').expect
var app = require('../app')
var supertest = require('supertest')
var should = require('chai').should()
var appAgent = supertest.agent(app);

describe('Testing Fizz Buzz Controller', function () {
	describe('When the request param "count" is less than or equal to 0', function () {
		it('should return a 400 and a single error message', function (done) {
			
			appAgent
			.get('/fizzBuzz/0')
			.expect(400)
			.end(function (err, res) {
				expect(res.statusCode).to.equal(400);
				var responseJson = JSON.parse(res.text)
				responseJson[0].messages.length.should.equal(1)
				done()
			})
		})
	})

	describe('When the request param "count" is not a number', function () {
		it('should return a 400 and a single error message', function (done) {
			
			appAgent
			.get('/fizzBuzz/abc')
			.expect(400)
			.end(function (err, res) {
				expect(res.statusCode).to.equal(400);
				var responseJson = JSON.parse(res.text)
				responseJson[0].messages.length.should.equal(1)
				done()
			})
		})
	})

	describe('When the request param "count" is a number greater than 0', function () {
		it('should return a 200 and an array of result', function (done) {
			var count = 100
			appAgent
			.get(`/fizzBuzz/${count}`)
			.expect(200)
			.end(function (err, res) {
				expect(res.statusCode).to.equal(200);
				var responseJson = JSON.parse(res.text)
				responseJson.length.should.equal(count)
				done()
			})
		})
	})
})