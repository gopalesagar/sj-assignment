var express = require('express')
var router = express.Router()
var _ = require('lodash')
var validate = require('express-validation')
var fizzBuzzValidation = require('../test/validation/fizzBuzz')

/**
 * count: Count till which fizz buzz list is required
 */
router.get('/:count', validate(fizzBuzzValidation), function(req, res, next) {
  var count = req.params.count ? parseInt(req.params.count) : 0
  let fizzBuzzList = [];
  for(let i = 1; i <= count; i++) {
    let fizzBuzzString = ''
    if (i%3 === 0) fizzBuzzString = fizzBuzzString + 'Fizz'
    if (i%5 === 0) fizzBuzzString = fizzBuzzString + 'Buzz'
    if(_.isEmpty(fizzBuzzString)) fizzBuzzString = i

    fizzBuzzList.push(fizzBuzzString)
  }
  res.status(200).send(fizzBuzzList)
})

module.exports = router;
